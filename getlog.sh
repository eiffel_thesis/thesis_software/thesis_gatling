#! /usr/bin/env bash
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0


# Harvard dataverse provides an access log for zanbil.ir on this URL:
# https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/3QBYB5&version=1.0
# zanbil.ir is an Iranian web store which sells everything.
# The access log are nginx log from January 22th 2019 to January 26th 2019.
LOG_URL='https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/3QBYB5/NXKB6J'
WGET_OUPUT='access.zip'

# Effectively get the log archive.
wget $LOG_URL --output-document=$WGET_OUPUT

# Unzip the log archive to obtain access.log.
# This file can now be given to log2json.pl to be translated to JSON.
unzip $WGET_OUPUT

# Clean the __MACOSX.
rm -r __MACOSX