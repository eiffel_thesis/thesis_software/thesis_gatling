/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import scala.io.Source
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.ScenarioCSVReader
import woocommerce.common.ScenarioJSONParser


/**
 * Test the scenario JSON parser.
 */
class TestScenarioJSONParser extends Simulation{
	/**
	 * This CSV file contains products names.
	 *
	 * It was obtained by first running those commands to add products:
	 * wp cyclone seed --allow-root
	 * wp cyclone products 120 --allow-root
	 *
	 * The product names are obtained with the following command and stored into
	 * CSV file:
	 * wp db query "select post_name from wp_posts where post_type = 'product';" --allow-root
	 *
	 * CSV file looks like:
	 * product_name
	 * in-accusamus
	 * incidunt-qui
	 * tenetur-rerum
	 */
	final val FACTORY_PRODUCTS_FILE = "user-files/resources/TestScenarioJSONParserProducts.csv"

	/**
	 * This CSV file contains usernames and their displaynames.
	 *
	 * We first add users by adding orders:
	 * wp cyclone orders 30 --allow-root
	 *
	 * We get the user names and the associated display name by using this
	 * command:
	 * wp db query "select user_login, display_name from wp_users;" --allow-root
	 *
	 * Those data are stored into CSV file which looks like:
	 * username,displayname
	 * chet.cruickshank,Margie Schumm
	 * kip.klocko,Xzavier Haag
	 * laurie.kuvalis,Frances Kling
	 */
	final val FACTORY_USERS_FILE = "user-files/resources/TestScenarioJSONParserUsers.csv"

	/**
	 * This file contains our scenario and will be parsed.
	 */
	final val JSON_FILE = "user-files/resources/TestScenarioJSONParser.json"

	val scenarioCSVReader = new ScenarioCSVReader(FACTORY_USERS_FILE, FACTORY_PRODUCTS_FILE)

	val productsNames = scenarioCSVReader.parseProducts()

	// Access the returned tuple
	val (userNames, displayedNames) = scenarioCSVReader.parseUsers()

	/**
	 * The equation to set FACTORY_LAST_PAGE_NUMBER is:
	 * FACTORY_LAST_PAGE_NUMBER = number_of_products / 12 + number_of_products % 12 ? 1 : 0
	 * Because there are 12 products per page and always one page even if there
	 * are less than 12 products.
	 */
	final val FACTORY_LAST_PAGE_NUMBER = 10

	/**
	 * FACTORY_LAST_PRODUCT_ID was set by using:
	 * wp db query "select id from wp_posts where post_type = 'product';" --allow-root
	 * It seems that ID are always even and begins from 247.
	 * The last one was captured with the SQL above.
	 */
	final val FACTORY_LAST_PRODUCT_ID = 247

	// Read the whole file at once.
	final val JSON = Source.fromFile(JSON_FILE).mkString

	// We instantiate a JSON Parser for those dummy data.
	val scenarioJSONParser = new ScenarioJSONParser(userNames, displayedNames, productsNames, FACTORY_LAST_PAGE_NUMBER, FACTORY_LAST_PRODUCT_ID)

	setUp(scenarioJSONParser.scenarioJSONParser(JSON))
}