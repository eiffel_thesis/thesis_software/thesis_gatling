/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.io.Source
import woocommerce.common.ScenarioCSVReader
import woocommerce.common.ScenarioJSONParser


/**
 * Run the experiment.
 */
class ExperimentScenario extends Simulation{
	/*
	 * The two following CSV files correspond to a database with 30000 products,
	 * 5000 users, 5000 orders and 5000 reviews.
	 * The database is about 200MB.
	 */
	final val USERS_FILE = "user-files/resources/ExperimentUsers.csv"
	final val PRODUCTS_FILE = "user-files/resources/ExperimentProducts.csv"
	final val FACTORY_LAST_PAGE_NUMBER = 2500
	final val FACTORY_LAST_PRODUCT_ID = 60007

	/*
	 * This JSON file is obtained from zanbil.ir access log which are translated
	 * to JSON format with log2json.pl
	 */
	final val JSON_FILE = "user-files/resources/Experiment.json"

	val scenarioCSVReader = new ScenarioCSVReader(USERS_FILE, PRODUCTS_FILE)

	val productsNames = scenarioCSVReader.parseProducts()

	// Access the returned tuple
	val (userNames, displayedNames) = scenarioCSVReader.parseUsers()

	// We instantiate a JSON Parser for those dummy data.
	val scenarioJSONParser = new ScenarioJSONParser(userNames, displayedNames, productsNames, FACTORY_LAST_PAGE_NUMBER, FACTORY_LAST_PRODUCT_ID)

	/*
	 * We read JSON_FILE.
	 * It will be parsed by ScenarioJSONParser and the users scenarios executed.
	 */
	val json = Source.fromFile(JSON_FILE).mkString

	setUp(scenarioJSONParser.scenarioJSONParser(json))
}