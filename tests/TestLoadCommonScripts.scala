/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.common.HttpProtocol
import woocommerce.common.LoadCommonScripts


/**
 * Test the requests defined in the LoadCommonScripts object.
 */
class TestLoadCommonScripts extends Simulation{
	val scn = scenario("Test Load Common Scripts").exec(LoadCommonScripts.loadCommonScripts)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}