/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.Connect
import woocommerce.scenarios.SeeProduct
import woocommerce.scenarios.AddReview
import woocommerce.common.HttpProtocol


/**
 * Test the scenario defined in the AddReview class as guest.
 */
class TestAddReviewGuest extends Simulation{
	/*
	 * This dummy product was created with following command:
	 * wp wc product create --name=gatling --user=wordpress --allow-root
	 */
	final val PRODUCT = "gatling"

	val seeProduct = new SeeProduct(PRODUCT)
	// Set parameter as false to insert review as guest.
	val addReview = new AddReview(true)

	val scn = scenario("Test Add Review (guest)").exec(seeProduct.seeProduct, addReview.addReview)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}