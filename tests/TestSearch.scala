/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.Search
import woocommerce.common.HttpProtocol


/**
 * Test the scenario defined in the Search object.
 */
class TestSearch extends Simulation{
	final val SEARCH_STRING = "pomme"

	val search = new Search(SEARCH_STRING)

	val scn = scenario("Test Search").exec(search.search)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}