/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.SeeCart
import woocommerce.common.HttpProtocol


/**
 * Test the scenario defined in the SeeCart object.
 */
class TestSeeCart extends Simulation{
	val scn = scenario("Test See Cart").exec(SeeCart.seeCart)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}