/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.Connect
import woocommerce.scenarios.SeeOrders
import woocommerce.scenarios.SeeOrder
import woocommerce.common.HttpProtocol
import Array.concat


/**
 * Test the scenario defined in the SeeOrder class.
 */
class TestSeeOrder extends Simulation{
	/*
	 * This dummy username needs to have been created.
	 * I created it with the following command:
	 * wp user create gatling gatling@gatling.org --user_pass=gatling --display_name="Richard Gatling" --allow-root
	 */
	final val USERNAME = "gatling"
	final val PASSWORD = "gatling"
	final val DISPLAY_NAME = "Richard Gatling"

	val connect = new Connect(USERNAME, PASSWORD, DISPLAY_NAME)

	var i = 0

	/*
	 * Create an array of ChainBuilder which will be given as argument for exec of
	 * scenario.
	 */
	var chain = new Array[ChainBuilder](0)

	/*
	 * Add the two first actions to the chain (connect.connect and
	 * SeeOrders.seeOrders).
	 * We use for that a concatenation of chain and another Array with the two
	 * first actions.
	 */
	chain = concat(chain, Array(connect.connect, SeeOrders.seeOrders))

	/*
	 * We then create 3 dummy orders for Richard Gatling with the following
	 * command:
	 * for i in {0..2}; do
	 * wp wc shop_order create --customer_id=2 --user=wordpress --allow-root
	 * done
	 *
	 * We will need to realize scenario SeeOrders so orders ID will be stored in
	 * the sesssion.
	 * We then take the three orders ID.
	 *
	 * We add the action to the chain.
	 */
	for(i <- 0 to 2){
		val seeOrder = new SeeOrder("${orders_ids(%d)}".format(i)).seeOrder

		chain = concat(chain, Array(seeOrder))
	}

	// Finally we use the chain as scenario's argument.
	val scn = scenario("Test See Order").exec(chain)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}