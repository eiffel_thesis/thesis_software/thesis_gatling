/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.scenarios._
import woocommerce.common.ScenarioFactory
import woocommerce.common.HttpProtocol


/**
 * Test the scenario factory.
 */
class TestScenarioFactory extends Simulation{
	/*
	 * The following constant will be used to test the ScenarioFactory.
	 * Since we will not run any scenario here but only test if the
	 * ScenarioFactory returns the expected object we do not care to create users,
	 * products or anything.
	 */
	final val FACTORY_USER_NAMES = Array("gatling")
	final val FACTORY_DISPLAYED_NAMES = Array("Richard Gatling")
	final val FACTORY_PRODUCT_NAMES = Array("gatling")
	final val FACTORY_LAST_PAGE_NUMBER = 100
	final val FACTORY_LAST_PRODUCT_ID = 100

	val scenarioFactory = new ScenarioFactory(FACTORY_USER_NAMES, FACTORY_DISPLAYED_NAMES, FACTORY_PRODUCT_NAMES, FACTORY_LAST_PAGE_NUMBER, FACTORY_LAST_PRODUCT_ID)

	// Test for each scenario in woocommerce.scenario.*.
	var obj = scenarioFactory.scenarioFactory("/browse/foo")
	assert(obj.isInstanceOf[BrowseCategory])

	obj = scenarioFactory.scenarioFactory("/filter/bar")
	assert(obj.isInstanceOf[BrowseProduct])

	obj = scenarioFactory.scenarioFactory("/login?user=quz")
	assert(obj.isInstanceOf[Connect])

	// Since Disconnect is a singleton we check the equality.
	obj = scenarioFactory.scenarioFactory("/logout?user=quz")
	assert(obj == Disconnect)

	obj = scenarioFactory.scenarioFactory("/search?search-string=book")
	assert(obj.isInstanceOf[Search])

	obj = scenarioFactory.scenarioFactory("/product?product-id=123")
	assert(obj.isInstanceOf[SeeProduct])

	obj = scenarioFactory.scenarioFactory("/basket/add?product-id=123")
	assert(obj.isInstanceOf[AddToCart])

	obj = scenarioFactory.scenarioFactory("/basket/checkout")
	assert(obj == SeeCart)

	obj = scenarioFactory.scenarioFactory("/order/list")
	assert(obj == SeeOrders)

	obj = scenarioFactory.scenarioFactory("/")
	assert(obj == SeeHome)

	obj = scenarioFactory.scenarioFactory("/order/track?order-number=123456")
	assert(obj.isInstanceOf[SeeOrder])

	obj = scenarioFactory.scenarioFactory("/order/create")
	assert(obj.isInstanceOf[Checkout])

	obj = scenarioFactory.scenarioFactory("/customerReview/save")
	assert(obj.isInstanceOf[AddReview])

	/*
	 * A request to /customer/profile in zanbil.ir prints all user information.
	 * In the factory we choose randomly between either SeeAccountDetails or
	 * SeeAccountAddress.
	 */
	obj = scenarioFactory.scenarioFactory("/customer/profile")
	assert(obj == SeeAccountDetails || obj == SeeAccountAddress)

	// See above comment.
	obj = scenarioFactory.scenarioFactory("/customer/saveBasicInfo")
	assert(obj.isInstanceOf[EditAccountDetails] || obj.isInstanceOf[EditAccountAddress])

	obj = scenarioFactory.scenarioFactory("/nothing")
	assert(obj == None)

	/*
	 * Copy/Paste of the gatling Basic Simulation scenario so gatling will be
	 * happy because we run something.
	 * Indeed we do not care about sending a request, we just want to test the
	 * above asserts.
	 * But gatling bitches if no requests are sent...
	 */
	val httpProtocol = http
		.baseUrl("http://computer-database.gatling.io") // Here is the root for all relative URLs
		.acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
		.doNotTrackHeader("1")
		.acceptLanguageHeader("en-US,en;q=0.5")
		.acceptEncodingHeader("gzip, deflate")
		.userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

	val scn = scenario("Scenario Name") // A scenario is a chain of requests and pauses
		.exec(
			http("request_1")
				.get("/")
		)

	setUp(scn.inject(atOnceUsers(1)).protocols(httpProtocol))
}