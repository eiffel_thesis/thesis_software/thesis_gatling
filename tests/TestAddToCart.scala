/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.Connect
import woocommerce.scenarios.SeeProduct
import woocommerce.scenarios.AddToCart
import woocommerce.common.HttpProtocol
import woocommerce.common.WooRandom


/**
 * Test the scenario defined in the AddToCart.
 */
class TestAddToCart extends Simulation{
	/*
	 * This dummy product was created with following command:
	 * wp wc product create --name=gatling --user=wordpress --regular_price=10 --allow-root
	 */
	final val PRODUCT = "gatling"
	final val QUANTITY = WooRandom.randomQuantity()

	// We first see the product to store its id in the session.
	val seeProduct = new SeeProduct(PRODUCT)
	val addToCart = new AddToCart("${post_id}", QUANTITY)

	val scn = scenario("Test Add To Cart").exec(seeProduct.seeProduct, addToCart.addToCart)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}