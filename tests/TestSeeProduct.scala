/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.SeeProduct
import woocommerce.common.HttpProtocol


/**
 * Test the scenario defined in the SeeProduct object.
 */
class TestSeeProduct extends Simulation{
	/*
	 * This dummy product was created with following command:
	 * wp wc product create --name=gatling --user=wordpress --allow-root
	 */
	final val PRODUCT = "gatling"

	val seeProduct = new SeeProduct(PRODUCT)

	val scn = scenario("Test See Product").exec(seeProduct.seeProduct)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}