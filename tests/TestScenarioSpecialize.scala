/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import woocommerce.scenarios._
import woocommerce.scenarios.SeeOrders
import woocommerce.scenarios.SeeOrder
import woocommerce.common.HttpProtocol
import woocommerce.common.ScenarioSpecialize
import woocommerce.common.ScenarioFactory


/**
 * Test the scenario specialise function.
 */
class TestScenarioSpecialize extends Simulation{
	/*
	 * Those constants are used by the scenario factory to create scenario.
	 * We used some commands before running this test.
	 *
	 * We first create a dummy user:
	 * wp user create gatling gatling@gatling.org --user_pass=cyclone --display_name="Richard Gatling" --allow-root
	 *
	 * We then create an order to Richard Gatling (Above command return user ID = 2):
	 * wp wc shop_order create --customer_id=2 --user=wordpress --allow-root
	 *
	 * We then add some product with those commands:
	 * wp cyclone seed --allow-root
	 * wp cyclone products 120 --allow-root
	 *
	 * We create this dummy product to be able to add it through our scenario:
	 * wp wc product create --name=gatling --regular_price=10 --user=wordpress --allow-root
	 *
	 * The equation to set FACTORY_LAST_PAGE_NUMBER is:
	 * FACTORY_LAST_PAGE_NUMBER = number_of_products / 12 + 1
	 * Because there are 12 products per page and always one page even if there
	 * are less than 12 products.
	 *
	 * FACTORY_LAST_PRODUCT_ID was set by using:
	 * wp db query "select id from wp_posts where post_type = 'product';" --allow-root
	 * It seems that ID are always odd and begins from 10.
	 * The last one was captured with the SQL above.
	 */
	final val FACTORY_USER_NAMES = Array("gatling")
	final val FACTORY_DISPLAYED_NAMES = Array("Richard Gatling")
	final val FACTORY_PRODUCT_NAMES = Array("gatling")
	final val FACTORY_LAST_PAGE_NUMBER = 11
	final val FACTORY_LAST_PRODUCT_ID = 250

	/*
	 * The user will:
	 * 1. Browse the categories.
	 * 2. Browse the product.
	 * 3. Search a product.
	 * 4. Connect.
	 * 5. See its account details or address.
	 * 6. Edit them.
	 * 7. See its orders.
	 * 8. See one order specially.
	 * 9. See a product (We need to see a product before adding a review).
	 * 10. Add a review to a product.
	 * 11. See a product.
	 * 12. Add a product to cart.
	 * 13. See its cart.
	 * 14. Buy its cart.
	 * 15. Disconnect.
	 * 16. See Home page.
	 */
	final val REQUESTS = Array("/browse", "/filter", "/search", "/login", "/customer/profile", "/customer/saveBasicInfo", "/order/list", "/order/track", "/product", "/customerReview/save", "/product", "/basket/add", "/basket/checkout", "/order/create", "/logout", "/")

	// We instantiate a factory for those dummy data.
	val scenarioFactory = new ScenarioFactory(FACTORY_USER_NAMES, FACTORY_DISPLAYED_NAMES, FACTORY_PRODUCT_NAMES, FACTORY_LAST_PAGE_NUMBER, FACTORY_LAST_PRODUCT_ID)

	// We will store all of our scenarios' exec in this array.
	var chain = new Array[ChainBuilder](0)

	for(request <- REQUESTS){
		/*
		 * We first get the associated woocommerce.scenarios object with the
		 * factory.
		 * We then specialize it with the scenario specialize to obtain the
		 * ChainBuilder.
		 */
		val exec = ScenarioSpecialize.scenarioSpecialize(scenarioFactory.scenarioFactory(request))

		// We can now add the ChainBuilder to the list.
		chain = Array.concat(chain, Array(exec))
	}

	/*
	 * Just before testing the whole scenario we test the default case of
	 * specialize.
	 * If the scenario can not be specialized the specialized returns null.
	 */
	assert(ScenarioSpecialize.scenarioSpecialize(scenarioFactory.scenarioFactory("foo")) == null)

	// Finally we use the chain as scenario's argument.
	val scn = scenario("Test Scenario Specialize").exec(chain)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}