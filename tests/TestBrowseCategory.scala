/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.BrowseCategory
import woocommerce.common.HttpProtocol
import woocommerce.common.WooRandom


/**
 * Test the scenario defined in the Browse object.
 */
class TestBrowseCategory extends Simulation{
	/*
	 * We create some products to have at least two pages of products in each
	 * category with the following command:
	 * wp cyclone seed --allow-root
	 * wp cyclone products 120 --allow-root
	 *
	 * We then choose a category randomly in possibles categories, which are:
	 * 'Drama', 'Mystery', 'Romance', 'Horror', 'Travel' and 'Health'.
	 */
	final val CATEGORY = WooRandom.randomCategory()
	final val PAGE_NUMBER = 2

	val browseCategory = new BrowseCategory(CATEGORY, PAGE_NUMBER)

	val scn = scenario("Test Browse Category").exec(browseCategory.browseCategory)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}