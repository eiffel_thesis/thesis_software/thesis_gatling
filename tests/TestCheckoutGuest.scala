/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.SeeProduct
import woocommerce.scenarios.AddToCart
import woocommerce.scenarios.Checkout
import woocommerce.common.HttpProtocol


/**
 * Test the scenario defined in the Checkout as guest.
 */
class TestCheckoutGuest extends Simulation{
	/*
	 * This dummy product was created with following command:
	 * wp wc product create --name=gatling --user=wordpress --regular_price=10 --allow-root
	 */
	final val PRODUCT = "gatling"
	final val QUANTITY = 1

	// We first see the product to store its id in the session.
	val seeProduct = new SeeProduct(PRODUCT)
	val addToCart = new AddToCart("${post_id}", QUANTITY)
	val checkout = new Checkout(true)

	val scn = scenario("Test Checkout (guest)").exec(seeProduct.seeProduct, addToCart.addToCart, checkout.checkout)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}