/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.BrowseProduct
import woocommerce.common.HttpProtocol


/**
 * Test the scenario defined in the Browse object.
 */
class TestBrowseProduct extends Simulation{
	/*
	 * We create 13 products to have at least two pages of products.
	 *
	 * The dummy products are created with the following commands:
	 * wp cyclone seed --allow-root
	 * wp cyclone products 13 --allow-root
	 */
	final val PAGE_NUMBER = 2

	val browseProduct = new BrowseProduct(PAGE_NUMBER)

	val scn = scenario("Test Browse Product").exec(browseProduct.browseProduct)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}