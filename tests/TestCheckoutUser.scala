/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.Connect
import woocommerce.scenarios.SeeProduct
import woocommerce.scenarios.AddToCart
import woocommerce.scenarios.Checkout
import woocommerce.common.HttpProtocol


/**
 * Test the scenario defined in the Checkout as user.
 */
class TestCheckoutUser extends Simulation{
	/*
	 * This dummy username needs to have been created.
	 * I created it with the following command:
	 * wp user create gatling gatling@gatling.org --user_pass=gatling --display_name="Richard Gatling" --allow-root
	 */
	final val USERNAME = "gatling"
	final val PASSWORD = "gatling"
	final val DISPLAY_NAME = "Richard Gatling"

	/*
	 * This dummy product was created with following command:
	 * wp wc product create --name=gatling --user=wordpress --regular_price=10 --allow-root
	 */
	final val PRODUCT = "gatling"
	final val QUANTITY = 1

	val connect = new Connect(USERNAME, PASSWORD, DISPLAY_NAME)

	// We first see the product to store its id in the session.
	val seeProduct = new SeeProduct(PRODUCT)
	val addToCart = new AddToCart("${post_id}", QUANTITY)

	// Checkout as user instead of guest.
	val checkout = new Checkout(false)

	val scn = scenario("Test Checkout (user)").exec(
		connect.connect,
		seeProduct.seeProduct,
		addToCart.addToCart,
		checkout.checkout
	)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}