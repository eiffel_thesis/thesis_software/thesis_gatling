/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.Connect
import woocommerce.scenarios.SeeProduct
import woocommerce.scenarios.AddReview
import woocommerce.common.HttpProtocol


/**
 * Test the scenario defined in the AddReview class with a connected user.
 */
class TestAddReviewUser extends Simulation{
	/*
	 * This dummy username needs to have been created.
	 * I created it with the following command:
	 * wp user create gatling gatling@gatling.org --user_pass=gatling --display_name="Richard Gatling" --allow-root
	 */
	final val USERNAME = "gatling"
	final val PASSWORD = "gatling"
	final val DISPLAY_NAME = "Richard Gatling"

	/*
	 * This dummy product was created with following command:
	 * wp wc product create --name=gatling --user=wordpress --allow-root
	 */
	final val PRODUCT = "gatling"

	val connect = new Connect(USERNAME, PASSWORD, DISPLAY_NAME)
	val seeProduct = new SeeProduct(PRODUCT)
	// Set parameter as false to insert review as user.
	val addReview = new AddReview(false)

	val scn = scenario("Test Add Review (user)").exec(connect.connect, seeProduct.seeProduct, addReview.addReview)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}