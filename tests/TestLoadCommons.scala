/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.common.HttpProtocol
import woocommerce.common.LoadCommons
import woocommerce.scenarios.SeeHome


/**
 * Test the requests defined in the LoadCommons object.
 */
class TestLoadCommons extends Simulation{
	/*
	 * Create an array of ChainBuilder which will be given as argument for exec of
	 * scenario.
	 */
	var chain = Array(SeeHome.seeHome, LoadCommons.loadCommons, SeeHome.seeHome, LoadCommons.loadCommons)

	/*
	 * If LoadCommons works correctly the commons should be requests only once
	 * while the home page should be requested twice.
	 */
	val scn = scenario("Test Load Commons").exec(chain)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}