/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.Connect
import woocommerce.scenarios.SeeAccountAddress
import woocommerce.scenarios.EditAccountAddress
import woocommerce.common.HttpProtocol


/**
 * Test the scenario defined in the EditAccountAddress class to edit the
 * shipping address.
 */
class TestEditAccountShippingAddress extends Simulation{
	/*
	 * This dummy username needs to have been created.
	 * I created it with the following command:
	 * wp user create gatling gatling@gatling.org --user_pass=gatling --display_name="Richard Gatling" --allow-root
	 */
	final val USERNAME = "gatling"
	final val PASSWORD = "gatling"
	final val DISPLAY_NAME = "Richard Gatling"

	val connect = new Connect(USERNAME, PASSWORD, DISPLAY_NAME)

	// Set argument to false to edit shipping address.
	val editAccountAddress = new EditAccountAddress(false)

	val scn = scenario("Test Edit Shipping Address").exec(connect.connect, SeeAccountAddress.seeAccountAddress, editAccountAddress.editAccountAddress)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}