/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import woocommerce.scenarios.SeeHome
import woocommerce.common.HttpProtocol


/**
 * Test the scenario defined in the SeeHome object.
 */
class TestSeeHome extends Simulation{
	val scn = scenario("Test See Home").exec(SeeHome.seeHome)

	setUp(scn.inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol))
}