#! /usr/bin/env bash
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0

# Gatling resources directory is in GATLING_ROOT/user_files/resources
# And this git repository is in GATLING_ROOT/user_files/simulation
GATLING_RESOURCES_DIR="$(pwd)/../.."

# Create a symbolic link for all resources files to GATLING_RESOURCES_DIR.
for i in resources/*; do
	ln -rs $i $GATLING_RESOURCES_DIR/$i
done