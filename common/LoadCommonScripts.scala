/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.common
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
 * Simply do GET requests to all common JS files.
 */
object LoadCommonScripts{
	// Do simple get on all common scripts (i.e. the scripts needed on all pages).
	val loadCommonScripts = exec(
		http("Load Common Script 1").
		get("/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp").
		check(regex("jQuery v1.12.4"))
	).
	exec(
		http("Load Common Script 2").
		get("/wp-includes/js/jquery/jquery-migrate.js?ver=1.4.1").
		check(regex("jQuery Migrate - v1.4.1"))
	).
	exec(
		http("Load Common Script 3").
		get("/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.js?ver=2.70").
		check(regex("jQuery blockUI plugin"))
	).
	exec(
		http("Load Common Script 4").
		get("/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.js?ver=3.8.1").
		check(regex("global wc_add_to_cart_params"))
	).
	exec(
		http("Load Common Script 5").
		get("/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.js?ver=2.1.4").
		check(regex("JavaScript Cookie v2.1.4"))
	).
	exec(
		http("Load Common Script 6").
		get("/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.js?ver=3.8.1").
		check(regex("global Cookies"))
	).
	exec(
		http("Load Common Script 7").
		get("/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.js?ver=3.8.1").
		check(regex("global wc_cart_fragments_params, Cookies"))
	).
	exec(
		http("Load Common Script 8").
		get("/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.js?ver=3.8.1").
		check(regex("global wc_cart_fragments_params, Cookies"))
	).
	exec(
		http("Load Common Script 9").
		get("/wp-content/themes/storefront/assets/js/navigation.js?ver=2.5.3").
		check(regex("global storefrontScreenReaderText"))
	).
	exec(
		http("Load Common Script 10").
		get("/wp-content/themes/storefront/assets/js/skip-link-focus-fix.js?ver=20130115").
		check(regex("function"))
	).
	exec(
		http("Load Common Script 11").
		get("/wp-content/themes/storefront/assets/js/vendor/pep.min.js?ver=0.4.3").
		check(regex("PEP v0.4.3 | https://github.com/jquery/PEP"))
	).
	exec(
		http("Load Common Script 12").
		get("/wp-content/themes/storefront/assets/js/woocommerce/header-cart.js?ver=2.5.3").
		check(regex("Makes the header cart content scrollable if the height of the dropdown exceeds the window height."))
	).
	exec(
		http("Load Common Script 13").
		get("/wp-content/themes/storefront/assets/js/footer.js?ver=2.5.3").
		check(regex("footer.js"))
	).
	exec(
		http("Load Common Script 14").
		get("/wp-includes/js/wp-embed.js?ver=5.3.2").
		check(regex("WordPress inline HTML embed"))
	).
	exec(
		http("Load Common Script 15").
		get("/wp-includes/js/twemoji.js?ver=5.3.2").
		check(regex("jslint indent: 2"))
	).
	exec(
		http("Load Common Script 16").
		get("/wp-includes/js/wp-emoji.js?ver=5.3.2").
		check(regex("wp-emoji.js is used"))
	)
}