/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.common
import scala.io.Source
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
 * This class provides methods to read CSV files which contains information
 * about products and users.
 * @param usersFile The CSV file which contains information about users.
 * @param productsFile The CSV file which contains information about products.
 */
class ScenarioCSVReader(usersFile: String, productsFile: String){
	/**
	 * Parse the usersFile and store the usernames and display names into
	 * arrays.
	 * @return A tuple of two arrays whom first contains all usernames and second
	 * all displaynames.
	 */
	def parseUsers(): (Array[String], Array[String]) = {
		var logins = new Array[String](0)
		var displaynames = new Array[String](0)

		val bufferedSource = Source.fromFile(usersFile)

		for(line <- bufferedSource.getLines.drop(1)){ // drop(1) to drop the header.
			/*
			 * The following snippet was taken from:
			 * https://alvinalexander.com/scala/csv-file-how-to-process-open-read-parse-in-scala
			 * It first open the file then read it line by line and split each line
			 * into a map.
			 * The different column can be accessed through the map.
			 */
			val Array(login, displayname) = line.split(",").map(_.trim)

			// Add the column to the corresponding array.
			logins = Array.concat(logins, Array(login))
			displaynames = Array.concat(displaynames, Array(displayname))
		}

		// Return the two array inside a tuple.
		return (logins, displaynames)
	}

	/**
	 * Parse the productsFile and store all its items into an array
	 * @return An array of product names.
	 */
	def parseProducts(): Array[String] = {
		var products = new Array[String](0)

		val bufferedSource = Source.fromFile(productsFile)

		for(line <- bufferedSource.getLines){
			/*
			 * Since there is only one column in our CSV file we can add the whole
			 * line.
			 */
			products = Array.concat(products, Array(line))
		}

		return products
	}
}