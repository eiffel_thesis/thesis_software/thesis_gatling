/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.common
import scala.util.Random


/**
 * This object contains functions to randomly generate things.
 */
object WooRandom{
	/**
	 * The minimal string length will be 10 characters.
	 */
	final val MINIMAL_STRING_LENGTH = 10

	/**
	 * This value is used to generate a random string whom length will be in
	 * [MINIMAL_STRING_LENGTH, MINIMAL_STRING_LENGTH + RANDOM_LENGTH].
	 */
	final val RANDOM_LENGTH = 5

	// Our books belong to the following categories.
	final val CATEGORIES = Array("Drama", "Mystery", "Romance", "Horror", "Travel", "Health")

	/*
	 * Apparently the first product ID in WooCommerce is 10.
	 * Then the product ID are odd numbers beginning from 10...
	 */
	final val FIRST_PRODUCT_ID = 10

	/**
	 * Generate a random alphanumeric string whom length is in
	 * [MINIMAL_STRING_LENGTH, MINIMAL_STRING_LENGTH + RANDOM_LENGTH]
	 * @return A random string.
	 */
	def randomString(): String = {
		return Random.alphanumeric.take(MINIMAL_STRING_LENGTH + Random.nextInt(RANDOM_LENGTH)).mkString
	}

	/**
	 * Generate a random mail address.
	 * Basically it just a random string which ends with "@random.org".
	 * @return A String which contains a mail address.
	 */
	def randomMailAddress(): String = {
		return "%s@ramdom.org".format(randomString())
	}

	/**
	 * Generate a random rating in [1, 5].
	 * @return A String which contains something in [1, 5].
	 */
	def randomRating(): String = {
		return "%d".format(Random.nextInt(5) + 1)
	}

	/**
	 * Generate a random French ZIP code.
	 *
	 * French ZIP codes have 5 numbers.
	 * It is not possible to have postcode inferior to 01000 but WooCommerce
	 * seems to accept the ZIP code 00000 so the following snippet is OK.
	 * The snippet will generate a random number in [0, 99999] and add 0 so
	 * the string length is 5 (e.g., 5 will be expand to 00005).
	 *
	 * @return A String which contains a French ZIP code.
	 */
	def randomFrenchPostCode(): String = {
		return "%05d".format(Random.nextInt(99999))
	}

	/**
	 * Generate a random French phone number.
	 *
	 * French phone numbers have 10 numbers.
	 *
	 * @return A String which contains a French phone number.
	 */
	def randomPhoneNumber(): String = {
		return "%010d".format(Random.nextInt())
	}

	/**
	 * Choose randomly a category in {"Drama", "Mystery", "Romance", "Horror",
	 * "Travel", "Health"}.
	 * @return A String which contains a randomly chosen category
	 */
	def randomCategory(): String = {
		return CATEGORIES(Random.nextInt(CATEGORIES.length))
	}

	/**
	 * Generate a random page number in [1, lastPage].
	 * @param lastPage The number of the last page of product.
	 * @return A random page number in [1, lastPage]
	 */
	def randomPageProduct(lastPage: Int): Int = {
		return Random.nextInt(lastPage) + 1
	}

	/**
	 * Generate a random page number in one category.
	 * NOTE This function makes the assumption that the categories have
	 * approximately the same number of product.
	 * @param lastPage The value of the last page of the product page.
	 * @return A random page number in one category. Basically it will be randomly
	 * chosen in [1, lastPage / numberOfCategories].
	 */
	def randomPageCategory(lastPage: Int): Int = {
		/*
		 * If there is less page than categories there will be one page per
		 * categories with possibly empty category page.
		 */
		if(lastPage < WooRandom.CATEGORIES.length)
			return 1

		/*
		 * If the categories have the same number of product the number of page per
		 * category equals:
		 * last_page_number / number_of_categories + 1
		 * Since there is at least one page we need to add 1.
		 */
		return Random.nextInt(lastPage / WooRandom.CATEGORIES.length) + 1
	}

	/**
	 * Generate a random product ID, that is to say an odd number in
	 * [FIRST_PRODUCT_ID, lastProductID].
	 * Indeed, product ID in WooCommerce seems to be odd.
	 * @param lastProductID The biggest product ID.
	 * @return An odd number in [FIRST_PRODUCT_ID, lastProductID]
	 */
	def randomProductId(lastProductID: Int): Int = {
		// Generate a random number in [0, lastProductID - FIRST_PRODUCT_ID]
		var id = Random.nextInt(lastProductID - FIRST_PRODUCT_ID)

		// If the number is odd we add 1 to make it even.
		if(id % 2 == 0)
			id += 1

		/*
		 * Correct the range from [0, lastProductID - FIRST_PRODUCT_ID] to
		 * [FIRST_PRODUCT_ID, lastProductID].
		 */
		return id + FIRST_PRODUCT_ID
	}

	/**
	 * Generate a random number following a Poisson distribution whom lambda is
	 * given as argument.
	 *
	 * The algorithm was apparently invented by Donald E. Knuth and taken from:
	 * https://en.wikipedia.org/wiki/Poisson_distribution#Generating_Poisson-distributed_random_variables
	 *
	 * @param lambda The lambda of a Poisson distribution is the number which will
	 * be obtained on average.
	 * For example, poissonRandomNumber(1) will very often return 1 and
	 * poissonRandomNumber(5) will very often return 5.
	 * @return A random number which equals, on average, lambda.
	 */
	private def poissonRandomNumber(lambda: Int): Int = {
		val L = Math.exp(-lambda)
		var k = 0
		var p = 1.0

		do{
			k += 1
			p *= Random.nextDouble()
		}while(p > L)

		return k - 1
	}

	/**
	 * Generate a random quantity of product.
	 *
	 * The quantity is not chosen as a random uniform number but instead as a
	 * Poisson random number with lambda equals 1.
	 * The probability distribution will be:
	 * 1: .368
	 * 2: .368
	 * 3: .184
	 * 4: .061
	 * 5: .015
	 * 6: .003
	 * 7: .0005
	 * ...
	 *
	 * @return An integer which is a Poisson randomly chosen number which has
	 * great chance to be 1 or 2.
	 */
	def randomQuantity(): Int = {
		return poissonRandomNumber(1) + 1
	}
}