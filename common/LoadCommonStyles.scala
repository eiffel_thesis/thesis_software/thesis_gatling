/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.common
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
 * Simply do GET requests to all common CSS files.
 */
object LoadCommonStyles{
	// Do simple get on all common CSS (i.e. the CSS files needed on all pages).
	val loadCommonStyles = exec(
		http("Load Common Style 1").
		get("/wp-includes/css/dist/block-library/style.css?ver=5.3.2").
		check(regex(".wp-block-audio audio"))
	).
	exec(
		http("Load Common Style 2").
		get("/wp-includes/css/dist/block-library/theme.css?ver=5.3.2").
		check(regex(".wp-block-code"))
	).
	exec(
		http("Load Common Style 3").
		get("/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=1578906682").
		check(regex("@-webkit-keyframes"))
	).
	exec(
		http("Load Common Style 4").
		get("/wp-content/themes/storefront/style.css?ver=2.5.3").
		check(regex("Theme Name:"))
	).
	exec(
		http("Load Common Style 5").
		get("/wp-content/themes/storefront/assets/css/base/icons.css?ver=2.5.3").
		check(regex("Font Awesome Free"))
	).
	exec(
		http("Load Common Style 6").
		get("/wp-content/themes/storefront/assets/css/woocommerce/woocommerce.css?ver=2.5.3").
		check(regex("WooCommerce"))
	).
	exec(
		http("Load Common Style 7").
		get("/wp-content/themes/storefront/assets/css/base/gutenberg-blocks.css?ver=2.5.3").
		check(regex("Front-end only styles"))
	)
}