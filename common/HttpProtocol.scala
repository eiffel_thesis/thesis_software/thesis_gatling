/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.common
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
 * Object used to store a default HttpProtocol used for tests.
 */
object HttpProtocol{
	// WooCommerce container default port is 8080.
	final val PORT = 8080

	// WooCommerce container default url is localhost.
	final val URL = "localhost"

	val httpProtocol = http
		.baseUrl("http://%s:%d".format(URL, PORT))
		.acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
		.doNotTrackHeader("1")
		.acceptLanguageHeader("en-US,en;q=0.5")
		.acceptEncodingHeader("gzip, deflate")
		.userAgentHeader("Gatling") // Set Gatling as userAgent to clearly see Gatling requests in the log.
}