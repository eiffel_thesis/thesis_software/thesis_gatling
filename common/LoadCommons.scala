/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.common
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
 * Do GET requests to all styles and scripts files which are on any page of the
 * site.
 */
object LoadCommons{
	/*
	 * The following block of code will only load the commons styles and scripts
	 * if they were never loaded.
	 * To do so, we test if a session variable exists (loaded_common).
	 * If it does not exist we will load the commons and set this variable to
	 * something (we do not care about the value only about the fact it is
	 * defined).
	 */
	val loadCommons = doIf("${loaded_common.isUndefined()}"){
		// Load the commons.
		exec(LoadCommonStyles.loadCommonStyles).
		exec(LoadCommonScripts.loadCommonScripts).
		/*
		 * And define loaded_common so the commons will not be loaded for this
		 * session.
		 * It seems that current session in Gatling is stored in "_" variable.
		 * I found the information not in the Gatling documentation but here:
		 * https://stackoverflow.com/a/31809137
		 */
		exec(_.set("loaded_common", 1))
	}
	/*
	 * The following snippet can be used to see that after the above block
	 * loaded_common is set to 1.
	 */
// 	.exec { session =>
// 		println("loaded_common is '" + session("loaded_common").as[String] + "'")
// 		session
// 	}
}