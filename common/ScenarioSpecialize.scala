/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.common
import io.gatling.core.structure.ChainBuilder
import woocommerce.scenarios._


/**
 * This object contains a function which, from an object in
 * woocommerce.scenarios returns the corresponding ChainBuilder.
 */
object ScenarioSpecialize{
	/**
	 * Returns the ChainBuilder from a given scenario
	 * @param obj The scenario that will be specialized.
	 * @return The ChainBuilder (i.e. exec) of the scenario will be returned.
	 */
	def scenarioSpecialize(obj: Object): ChainBuilder = obj match {
		/*
		 * If given argument obj has type BrowseCategory we cast it as
		 * BrowseCategory and access the exec browseCategory from this object.
		 * We do the same for all other scenarios.
		 */
		case _: BrowseCategory => obj.asInstanceOf[BrowseCategory].browseCategory
		case _: BrowseProduct => obj.asInstanceOf[BrowseProduct].browseProduct
		case _: Connect => obj.asInstanceOf[Connect].connect
		// Disconnect is an object so we just test for equality.
		case Disconnect => Disconnect.disconnect
		case _: SeeProduct => obj.asInstanceOf[SeeProduct].seeProduct
		case _: Search => obj.asInstanceOf[Search].search
		case SeeCart => SeeCart.seeCart
		case _: AddToCart => obj.asInstanceOf[AddToCart].addToCart
		case SeeAccountDetails => SeeAccountDetails.seeAccountDetails
		case SeeAccountAddress => SeeAccountAddress.seeAccountAddress
		case _: EditAccountDetails => obj.asInstanceOf[EditAccountDetails].editAccountDetails
		case _: EditAccountAddress => obj.asInstanceOf[EditAccountAddress].editAccountAddress
		case SeeOrders => SeeOrders.seeOrders
		case _: SeeOrder => obj.asInstanceOf[SeeOrder].seeOrder
		case SeeHome => SeeHome.seeHome
		case _: Checkout => obj.asInstanceOf[Checkout].checkout
		case _: AddReview => obj.asInstanceOf[AddReview].addReview
		case _ => null
	}
}