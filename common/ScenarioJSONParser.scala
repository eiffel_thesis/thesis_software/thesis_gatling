/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.common
import com.fasterxml.jackson.databind.ObjectMapper // Needed to use jackson.
import com.fasterxml.jackson.databind.JsonNode // Same as above.
import io.gatling.core.Predef._ // Needed to use scenario.
import io.gatling.http.Predef._ // Same as above.
import io.gatling.core.structure.ChainBuilder
import io.gatling.core.structure.PopulationBuilder
/*
 * Gatling uses Scala 2.12.
 * So in Scala 2.12 you need to use this import to convert Java Iterator to
 * Scala one.
 * The answer was taken from: https://stackoverflow.com/a/53747458
 *
 * WARN If Gatling changes the version of scala used this file will maybe not
 * compile anymore or at least not without warnings...
 */
import scala.collection.JavaConverters._


/**
 * This class provides a JSON parser to correlate zanbil.ir log given as JSON
 * to WooCommerce.
 * It uses ScenarioFactory and ScenarioSpecialize.
 */
class ScenarioJSONParser(users: Array[String], displayed: Array[String], products: Array[String], lastPage: Int, lastProductID: Int){
	/**
	 * Translates a String which represents the time (HH:MM:SS) to absolute time
	 * in seconds since midnight.
	 * For example, 09:10:12 will return 33012.
	 * @param time The hour in day given as HH:MM:SS to translate.
	 * @return The absolute time since midnight for the given time.
	 */
	private def absTimeInDay(time: String): Int = {
		val pattern = """(\d+):(\d+):(\d+)""".r

		// Get the hours, minutes and seconds in the given time.
		return time match {
			/*
			 * Since we want the result in seconds we need to translate things.
			 * First, we translate hour in seconds by multiplicating by 3600 (there are 3600
			 * seconds in an hour/).
			 * Then, we translate minutes in seconds multiplicating by 60 (there are 60
			 * seconds in a minute).
			 * Finally we add the remaining seconds.
			 */
			case pattern(hours, minutes, seconds) => hours.toInt * 3600 + minutes.toInt* 60 + seconds.toInt
			case _ => println("%s does not match regex".format(time)); 0
		}
	}

	/*
	 * We need to instantiate a ScenarioFactory to be able to correlate requests
	 * URL to corresponding woocommerce.scenarios object.
	 */
	private val factory = new ScenarioFactory(users, displayed, products, lastPage, lastProductID)

	/**
	 * Parses a string which contains JSON and create scenario for each IP in the
	 * JSON.
	 * The scenario consist of the request done by this IP.
	 * The JSON must look like something like this:
	 * {
	 * 	"255.32.0.1": [
	 * 		{"00:00:00": "/browse"},
	 * 		{"00:05:00": "/checkout"}
	 * 	],
	 * 	"127.0.52.63": [
	 * 		{"13:42:42": "/login"}
	 * 	]
	 * }
	 * @param json A string which contains JSON.
	 * @return A list of PopulationBuilder ready to be given to setUp to run the
	 * simulation.
	 */
	def scenarioJSONParser(json: String): List[PopulationBuilder] = {
		var scenarios = List[PopulationBuilder]()

		/*
		 * Gatling comes with Jackson JSON parser bundled.
		 * We will use to parse our JSON.
		 */
		var parser = new ObjectMapper

		var root = parser.readValue(json, classOf[JsonNode])

		var ip = ""

		/*
		 * We will loop through all IP in the JSON.
		 * We translate the Java Iterator to Scala one.
		 */
		for(ip <- asScalaIterator(root.fieldNames()).toSeq){
			var chain = new Array[ChainBuilder](0)

			var nodes = root.get(ip)

			if(!nodes.isArray()){
				println("Error")

				return null
			}

			var nodeRequest = null

			/*
			 * The first request happens at midnight.
			 * So we initialize the time to 0.
			 */
			var time = 0

			// For this IP we will loop through all of its requests
			for(nodeRequest <- asScalaIterator(nodes.elements()).toSeq){
				/*
				 * A request is a map (String, String) where the first String is the
				 * hour of the request and the second String the URL.
				 */
				var map = nodeRequest.fields()

				while(map.hasNext()){
					var m = map.next()

					/*
					 * Get the key which is the hour and translates it to absolute time
					 * since midnight.
					 */
					var nodeTime = absTimeInDay(m.getKey())

					/*
					 * Compute the difference between the last request time and the
					 * current time.
					 */
					var difference = nodeTime - time

					/*
					 * If the current request happens after the last request we will pause
					 * during the difference between the current and the last requests.
					 */
					if(difference > 0)
						chain = Array.concat(chain, Array(pause(difference)))

					// Update the last request time to current time.
					time = nodeTime

					/*
					 * Get the request URL from the map and correlates it with a
					 * woocommerce.scenario object.
					 */
					val scenario = factory.scenarioFactory(m.getValue().textValue())

					// Specialize the scenario and add it to the chain.
					chain = Array.concat(chain, Array(ScenarioSpecialize.scenarioSpecialize(scenario)))
				}
			}

			// Create a scenario for this IP and the chain of request.
			val scn = scenario(ip).exec(chain).inject(atOnceUsers(1)).protocols(HttpProtocol.httpProtocol)

			scenarios = List.concat(scenarios, (scn :: Nil))
		}

		return scenarios
	}
}