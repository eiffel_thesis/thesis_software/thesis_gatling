/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.common
import woocommerce.scenarios._
import scala.util.Random


/**
 * This class permits to create the good scenario according to a request string
 * (e.g. "/basket/checkout" will return a SeeCart object).
 *
 * @param users An array of username which will be randomly chosen to create
 * scenario which needs username (Connect).
 * @param displayed An array of display name which will be randomly chosen to
 * create scenario which needs display names (Connect and EditAccountDetails).
 * @param products An array of product names which will be randomly chosen to
 * create scenario which needs product names (SeeProduct).
 * @param lastPage The last page of the shop, it will be used to randomly choose
 * a page in [1, lastPage] to browse the website.
 * @param lastProductID The biggest ID of all product, it will be used to
 * randomly choose a product to add to cart.
 */
class ScenarioFactory(users: Array[String], displayed: Array[String], products: Array[String], lastPage: Int, lastProductID: Int){
	/*
	 * The above regex are used to correlate zanbil.ir requests with our platform.
	 * For example, the address to add a product to cart in zanbil.ir is
	 * "/backset/add".
	 * In this case our factory will return an AddToCart object.
	 */
	final val BROWSE_CATEGORY = "(/browse).*".r
	final val BROWSE_PRODUCT = "(/filter).*".r
	final val CONNECT = "(/login).*".r
	final val DISCONNECT = "(/logout).*".r
	final val SEARCH = "(/search).*".r
	final val SEE_PRODUCT = "(/product).*".r
	final val ADD_TO_CART = "(/basket/add).*".r
	final val SEE_CART = "(/basket/checkout).*".r
	final val SEE_ACCOUNT_DETAILS = "(/customer/profile).*".r
	final val EDIT_ACCOUNT_DETAILS = "(/customer/saveBasicInfo).*".r
	final val SEE_ORDERS = "(/order/list).*".r
	final val SEE_ORDER = "(/order/track).*".r
	// We do not put .* here because the home page is just "/".
	final val SEE_HOME = "(/)".r
	final val CHECKOUT = "(/order/create).*".r
	final val ADD_REVIEW = "(/customerReview/save).*".r

	final val PASSWORD = "cyclone"

	if(users.length != displayed.length){
		Console.err.println("usernames array and displaynames array must have the same length!")

		// Maybe a bit brutal but I did not find something better...
		sys.exit()
	}

	val userNames = users
	val displayNames = displayed
	val productNames = products
	val lastPageNumber = lastPage
	val lastProductIDNumber = lastProductID

	/**
	 * Create the according Object from a given String.
	 * @param address The address of the request (e.g. "/basket/checkout").
	 * @return An Object which is in woocommerce.scenarios.*
	 */
	def scenarioFactory(address: String): Object = address match {
		/*
		 * Browse a random category and a random page inside the category.
		 * We make the assumption that there will be approximately the same number
		 * of product for each category so we can call WooRandom.randomPageCategory.
		 */
		case BROWSE_CATEGORY(_) => new BrowseCategory(WooRandom.randomCategory(), WooRandom.randomPageCategory(lastPage))
		case BROWSE_PRODUCT(_) => new BrowseProduct(WooRandom.randomPageProduct(lastPage))
		// Connect by getting randomly a user name in all the user name.
		case CONNECT(_) => val rand = Random.nextInt(userNames.length); new Connect(userNames(rand), PASSWORD, displayNames(rand))
		case DISCONNECT(_) => Disconnect
		// See a random product chosen in all the products
		case SEE_PRODUCT(_) => new SeeProduct(productNames(Random.nextInt(productNames.length)))
		// Search a random generated string.
		case SEARCH(_) => new Search(WooRandom.randomString())
		case SEE_CART(_) => SeeCart
		// Add a random chosen odd product ID in [10, last ID] with a random quantity.
		case ADD_TO_CART(_) => new AddToCart("%d".format(WooRandom.randomProductId(lastProductIDNumber)), WooRandom.randomQuantity())
		/*
		 * In zanbil.ir, the basic informations (firstname and lastname) are
		 * displayed on the same pages than the address.
		 * So we need to choose randomly if we will see either AccountDetails
		 * (firstname, * lastname, etc.) or AccountAddress.
		 */
		case SEE_ACCOUNT_DETAILS(_) => if(Random.nextFloat() < .5) SeeAccountDetails else SeeAccountAddress
		/*
		 * This case is the same as above.
		 * Expect that we need to randomly choose to edit either billing or shipping
		 * address.
		 */
		case EDIT_ACCOUNT_DETAILS(_) => if(Random.nextFloat() < .5) new EditAccountDetails(displayNames(Random.nextInt(displayNames.length))) else new EditAccountAddress(Random.nextFloat < .5)
		case SEE_ORDERS(_) => SeeOrders
		/*
		 * Get the first order ID from the session variable.
		 * Since this session list was shuffled it is the same as taking it
		 * randomly.
		 * Before I used "${orders_ids.random()}" but each time this string is used
		 * it would return a random id so it is not an expected behavior and caused
		 * bug in SeeOrder.
		 * Now the list is shuffled once and we always take the first element.
		 */
		case SEE_ORDER(_) => new SeeOrder("${orders_ids(0)}")
		case SEE_HOME(_) => SeeHome
		/*
		 * It seems impossible to buy as guest on zanbil.ir so each transaction on
		 * zanbil.ir will translate in guest or user transaction with a probability
		 * of .5.
		 */
		case CHECKOUT(_) => new Checkout(Random.nextFloat() < .5)
		// Same as above for guest.
		case ADD_REVIEW(_) => new AddReview(Random.nextFloat() < .5)
		case _ => None
	}
}