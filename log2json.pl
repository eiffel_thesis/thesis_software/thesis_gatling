#! /usr/bin/env perl
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;
use Getopt::Long;
use JSON::XS;


my $USAGE = "Usage: $0 [--day=23] [--requets=basket,browse,...] [--pretty] access.log";

# Try to match a line of access.log with a given regular expression.
#
# @param $line The line to parse
# @param $ip A scalar reference, if the line match the regex it will store the
# IP associated to the request.
# @param $hour A scalar reference, if the line matches the regex it will store
# the hour of the request.
# @param $request A scalar reference, if te line matches the regex it will store
# the URL of the request.
# @param $day The day of the request, request for other days will not be
# matched.
# @param @_ An array of accepted request URL.
# @return 1 if the line match, 0 otherwise.
sub match{
	my $line;
	my $day;
	my @requests;

	my $ip;
	my $hour;
	my $request;

	$line = shift @_ or die "match needs arguments!";
	$ip = shift @_ or die "match needs arguments!";
	$hour = shift @_ or die "match needs arguments!";
	$request = shift @_ or die "match needs arguments!";
	$day = shift @_ or die "match needs arguments!";
	@requests = @_;

	# If the request was not done the 23 January 2019 we filter it out.
	if($line !~ m-$day/Jan/2019-){
		return 0;
	}

	foreach my $req (@requests){
		if($line =~ m~(\d+\.\d+\.\d+\.\d+).*\[$day/Jan/2019:(\d+:\d+:\d+).*\] "(?:GET|POST) (?:/m)?(/$req.*) HTTP.*~){
			$$ip = $1;
			$$hour = $2;
			$$request = $3;

			return 1;
		}
	}

	return 0;
}

# Compare two time given as HH:MM:SS.
# The two parameters of this subroutine are $a and $b which are reference to
# hash which are like this:
# {time => request}
# This subroutine is used to sort the JSON so two requests which are done at the
# same time are effectively done at the same time.
sub compare_time{
	my $time_a;
	my $time_b;

	my $hour_a;
	my $minute_a;
	my $second_a;

	my $hour_b;
	my $minute_b;
	my $second_b;

	my $cmp;

	# The time is the key on the hash which contains onyl one key.
	$time_a = (keys %{$a})[0];
	$time_b = (keys %{$b})[0];

	# Split the time in HH, MM and SS.
	$time_a =~ /(\d+):(\d+):(\d+)/;
	$hour_a = $1;
	$minute_a = $2;
	$second_a = $3;

	$time_b =~ /(\d+):(\d+):(\d+)/;
	$hour_b = $1;
	$minute_b = $2;
	$second_b = $3;

	# We first compare the hours.
	$cmp = $hour_a <=> $hour_b;

	# If the hours are different we quit now.
	if($cmp){
		return $cmp;
	}

	# Otherwise we continue and compare the minutes.
	$cmp = $minute_a <=> $minute_b;

	if($cmp){
		return $cmp;
	}

	# Finally if the hours and minutes are the same we compare the seconds.
	return $second_a <=> $second_b;
}

my $filename;

my $fd;
my $line;

my %json;

my $day;
my @requests;
my $pretty;

# Set default values.
# We just want request for 23 January 2019.
$day = 23;
# We will just get the POST/GET requests on those URL.
@requests = ("basket/add", "basket/checkout", "browse", "customer/profile", "customer/saveBasicInfo", "filter", "login", "logout", "order/list", "order/track", "order/create", "product", "search");
# By default ugly JSON will be written to file.
$pretty = 0;

GetOptions("day=i" => \$day, "requests=s" => \@requests, "pretty" => \$pretty) or die $USAGE;

$filename = shift or die $USAGE;
open $fd, '<', $filename or die "Problem while openning $filename: $!";

while($line = <$fd>){
	my $ip;
	my $hour;
	my $request;

	# If the line matches the regex.
	if(match($line, \$ip, \$hour, \$request, $day, @requests)){
		# We add it to our hash.
		# Keys of our hash are IP and each IP points to an array of hash which are
		# (hour, request) tuple.
		if(exists $json{$ip}){
			push @{$json{$ip}}, {$hour => $request}
		}else{
			$json{$ip} = [{$hour => $request}]
		}
	}
}

# Sort the requests so requests done the same hour are effectively done the same
# hour.
# The only reason of this snippet is to permit merging of day to create more
# pressure.
# Otherwise if only one day is used or if merging day is not needed then this
# snippet is not needed.
foreach my $ip (keys %json){
	@{$json{$ip}} = sort compare_time @{$json{$ip}};
}

close $fd or warn "Problem while closing $filename: $!";

$filename =~ s/log/json/;

open $fd, '>', $filename or die "Problem while opening $filename: $!";

# We translate our hash to json and write it to file.
print $fd JSON::XS->new->pretty($pretty)->encode(\%json);

close $fd or warn "Problem while closing $filename: $!";

=pod

=head1 NAME

log2json

=head1 SYNOPSIS

	log2json.pl [-d 23] [--requests=home,search,...] access.log

=head1 DESCRIPTION

This script parses a webserver log an convert it to JSON.
Request for the same IP are grouped for this IP and sorted by hour.

The JSON generated looks like this one (if pretty flag is set):

{
	"255.32.0.1": [
		{
			"00:00:00": "/browse"
		},
		{
			"00:05:00": "/checkout"
		}
	],
	"127.0.52.63": [
		{
			"13:42:42": "/login"
		}
	]
}

=head1 OPTIONS

=over 4

=item B<-d|--day>

Requests added to JSON will be for this day only.

By default, requests are added for day equals 23 since this script was made
to parse access.log from https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/3QBYB5&version=1.0

=item B<--requests>

By default, the requests added to JSON will be requests which are made on those
URLs:

("/basket", "/browse", "/customer", "/filter", "/login", "/logout", "/order", "/product", "/search");

To change this behavior specifiy the URLs with this option.

=item B<--pretty>

Print pretty JSON (with spaces and line feeds) to file.

=back

=head1 VERSION

1.0

=head1 DEPENDENCIES

This perl script use Getopt::Long and JSON::XS modules.

=head1 AUTHOR

Francis Laniel <francis.laniel@lip6.fr>

=head1 LICENSE AND COPYRIGHT

This perl script is licensed under the term of the Mozilla Public License
version 2.0.

Author is the copyright owner.

=cut