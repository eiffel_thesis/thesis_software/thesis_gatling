# Gatling script to stress WooCommerce

This reposiroty contains a collection of gatling scripts to stress a WooCommerce fake website.

## Getting started

First we need to get gatling 3.2.1:

```bash
# Get gatling 3.2.1.
wget https://repo1.maven.org/maven2/io/gatling/highcharts/gatling-charts-highcharts-bundle/3.2.1/gatling-charts-highcharts-bundle-3.2.1-bundle.zip --output-document=gatling.zip
# Unzip it.
unzip gatling.zip
# Move thesis_gatling into correct directory.
mv thesis_gatling gatling-charts-highcharts-bundle-3.2.1/user-files/simulations/woocommerce
```

Then we need to get zanbil.ir log and translates them to JSON:

```bash
# Get the zanbil.ir access.log.
bash getlog.sh
# Translate to JSON.
perl log2json.pl access.log
# Move the JSON into good place.
mv access.json ../../resources/Experiment.json
```

After that run a woocommerce container, wait a bit and run gatling:

```bash
docker run -d -p80:8080 woocommerce
./bin/gatling.sh -nr -sf user-files/simulations/woocommerce -s woocommerce.tests.ExperimentScenario -rd "Experiment"
```

## What is this?

These scripts reproduce the behavior of the access.log of [zanbil.ir](https://www.zanbil.ir/) on a WooCommerce website.

The data loaded into WooCommerce are not that of zanbil.ir but fake books created with [wc-cyclone](https://github.com/metorikhq/wc-cyclone).

So, when in the access.log the url for zanbil.ir cart is read, a scenario to add a book to the cart is created with scala.

Basically, a `ScenarioJSONParser` parses a JSON files and creates scenario with a `ScenarioFactory`.
The scenario is converted to `ChainBuilder` by `ScenarioSpecialize`.

## Authors

**Francis Laniel** [<francis.laniel@lip6.fr>](francis.laniel@lip6.fr)

## License

This project is licensed under the Apache 2.0 License.