/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.LoadCommons


/**
 * Simulate a user connection
 * @param username The username of the user who will connect.
 * @param password The password of the user who will connect.
 * @param displayName The user displayname to check that the connection
 * succeeded.
 */
class Connect(val username: String, val password: String, val displayName: String){
	final val CONNECT_PAGE_ID = 8

	val connect = exec(LoadCommons.loadCommons)
	.exec(
		http("Get Connect page").
		get(s"/?page_id=$CONNECT_PAGE_ID"). // We first do a simple GET on the page to simulate user click.
		check(css("#woocommerce-login-nonce", "value").saveAs("login_nonce")) // We save the login nonce in the session to be able to retrieve it later.
	).
	doIf("${loaded_connect_scripts.isUndefined()}"){ // Load the script and style associated to this page only if they were not already loaded.
		exec(
			http("Get Connect Style").
			get("/wp-content/plugins/woocommerce/assets/css/select2.css?ver=3.8.1").
			check(regex(".select2-container"))
		).
		exec(
			http("Get Connect Script").
			get("/wp-content/plugins/woocommerce/assets/js/selectWoo/selectWoo.full.js?ver=1.0.6").
			check(regex("SelectWoo 1.0.6"))
		).
		/*
		 * Store the variable in the session so the script and style will not be
		 * reloaded for this session.
		 */
		exec(_.set("loaded_connect_scripts", 1))
	}.
	pause(5). // We then wait to let user type its username and password.
	exec(
		http(s"Do Connect ($username)").
		post(s"/?page_id=$CONNECT_PAGE_ID"). // Finally we connect the user.
		formParamMap(Map(
			"username" -> username,
			"password" -> password,
			/*
				* Here comes a try to explain this magic!
				* Gatling permits through its Expression Language to get things from
				* session.
				* So here we get the session value for key "nonce" that we set
				* previously to the login form nonce.
				* With the nonce we are now able to connect!
				*/
			"woocommerce-login-nonce" -> "${login_nonce}",
			"login" -> "Log in"
			)
		).
		check(regex(displayName)).
		check(regex("_wpnonce=(\\w+)").saveAs("logout_nonce")) // We also save the logout nonce to be able to disconnect later.
	)
}