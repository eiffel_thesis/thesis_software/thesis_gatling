/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
 * Add product to cart.
 * @param productId The ID of the product to add to cart.
 * @param quantity The quantity of product to add to cart.
 */
class AddToCart(val productId: String, val quantity: Int){
	val addToCart = exec(
		http(s"Add To Cart ($productId, $quantity)").
		post("/?wc-ajax=add_to_cart").
		formParamMap(Map(
			"product_id" -> productId,
			"quantity" -> quantity,
		)).
		/*
		 * Here comes the dark magic!
		 *
		 * The response body for request on /?wc-ajax seems to be JSON which
		 * contains elements to reload.
		 * So we want to check that elements to reload (the content of the cart)
		 * contains the just-added product ID.
		 * When there is an error a snippet of the JSON is, for example:
		 * data-product_id=\"31\"
		 *
		 * So it can easily be deduced that s"""data-product_id=\\"${productId}"""
		 * must match the thing!
		 * But no! It seems to have a bug with escaping and string interpolation in
		 * scala (https://github.com/scala/bug/issues/6476)!
		 *
		 * So we can think that "data-product_id=\\\"%s".format(productId) but no!
		 * So there is two possible either solution with are liste below!
		 *
		 * Welcome to the world of Scala!
		 */
// 		check(regex("""data-product_id=\\"%s""".format(productId)))
		check(regex(s"""data-product_id=."${productId}"""))
	)
}