/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.LoadCommons
import woocommerce.common.HttpProtocol


/**
 * Simply do a GET request to the search page.
 * @param searchString The string to search
 */
class Search(searchString: String){
	// Do a simple get on cart page.
	val search = exec(LoadCommons.loadCommons).
	exec(http(s"Search ($searchString)").
		get(s"/?s=$searchString&post_type=product").
		check(regex("h1.*Search results:.*%s".format(searchString))).
		check(
			css("""img[class~="attachment-woocommerce_thumbnail"]""", "src").
			findAll.
			transform(_.map(_.replaceFirst("http://(\\w+):%d".format(HttpProtocol.PORT), ""))).
			optional. // Maybe there is no product corresponding to this search.
			saveAs("search_images")
		)
	).
	doIf("${search_images.exists()}"){
		foreach("${search_images}", "image"){
			exec(
				http("Get search image: ${image}").
				get("${image}")
			)
		}
	}
}