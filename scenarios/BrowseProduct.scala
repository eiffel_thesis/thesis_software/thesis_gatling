/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.LoadCommons
import woocommerce.common.HttpProtocol


/**
 * Simply do a GET request on the page number.
 * @param pageNumber The page number to browse.
 */
class BrowseProduct(pageNumber: Integer){
	// Do a simple get on page pageNumber.
	val browseProduct = exec(LoadCommons.loadCommons).
	exec(
		http(s"Browse ($pageNumber)").
		get(s"/?post_type=product&paged=$pageNumber").
		// Check that the selected page number is equal to pageNumber.
		check(regex("Page %d".format(pageNumber))).
		check(
			/*
			 * Gatling css selector is based on:
			 * https://jodd.org/csselly/
			 * We want to get the image markup (<img>) whom class attribute contains
			 * "attachment-woocommerce_thumbnail".
			 * So, we do this by using class=~attachment-woocommerce_thumbnail.
			 */
			css("""img[class~="attachment-woocommerce_thumbnail"]""", "src").
			// We save all the URL into session variable.
			findAll.
			transform(_.map(_.replaceFirst("http://(\\w+):%d".format(HttpProtocol.PORT), ""))).
			saveAs("products_images")
		)
	).
	doIf("${products_images.exists()}"){
		/*
		 * We loop through all the images URL that we gathered above.
		 * We will use the session variable image to store the current URL.
		 */
		foreach("${products_images}", "image"){
			exec(
				http("Get product image: ${image}").
				get("${image}")
			)
		}
	}
}