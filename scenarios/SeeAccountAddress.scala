/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
 * Simply do a GET request to get the address page.
 * ALERT User needs to be connected to see this page!
 */
object SeeAccountAddress{
	final val CONNECT_PAGE_ID = 8

	// Do a simple get on addresses page.
	val seeAccountAddress = exec(
		http("See Account Addresses").
		get(s"/?page_id=$CONNECT_PAGE_ID&edit-address").
		check(regex("h1.*Addresses"))
	).
	doIf("${loaded_account_addresses_scripts.isUndefined()}"){ // Load these scripts only if they were not loaded before.
		exec(
			http("Get Account Addresses script 1").
			get("/wp-content/plugins/woocommerce/assets/js/frontend/country-select.js?ver=3.8.1").
			check(regex("global wc_country_select_params"))
		).
		exec(
			http("Get Account Addresses script 2").
			get("/wp-content/plugins/woocommerce/assets/js/frontend/address-i18n.js?ver=3.8.1").
			check(regex("global wc_address_i18n_params"))
		).
		exec(_.set("loaded_account_addresses_scripts", 1))
	}
}