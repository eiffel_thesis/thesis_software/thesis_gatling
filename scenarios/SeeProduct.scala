/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.LoadCommons
import woocommerce.common.HttpProtocol


/**
 * Simply do a GET request on a product.
 * @param product The product which will be get.
 */
class SeeProduct(val product: String){
	// Do a simple get on the product page.
	val seeProduct = exec(LoadCommons.loadCommons).
	exec(
		http(s"Get Product ($product)").
		get(s"/?product=$product").
		// We save the post id to be able to add a review later.
		check(css("#comment_post_ID", "value").saveAs("post_id")).
		check(regex(s"$product")).
		check(
			// We first save the related products and left and right product images.
			css("""img[class~="attachment-woocommerce_thumbnail"]""", "src").
			findAll.
			/*
			 * This transform applies the below transform (see below for detailled
			 * explanation) to all "other_products_images".
			 * Indeed, since we use findAll there will be numerous strings that we want
			 * to transform.
			 * These numerous strings are stored in something iterable that we will be
			 * accessed through _.
			 * We then apply a treatment to all _'s elements with function map().
			 *
			 * Information about this was taken from:
			 * https://groups.google.com/d/msg/gatling/zTlSB2MAhqU/Y2IthSnsIKIJ
			 */
			transform(_.map(_.replaceFirst("http://(\\w+):%d".format(HttpProtocol.PORT), ""))).
			saveAs("other_products_images")
		).
		check(
			// Then we save the viewed product image.
			css("""img[class~="wp-post-image"]""", "src").
			/*
			 * This transform removes the part of the URL before the "interesting"
			 * URL.
			 * For example, the following URL:
			 * http://localhost:8080/wp-content/uploads/2020/01/hdX0nbZ3HSI.jpg
			 * Will be transformed to:
			 * /wp-content/uploads/2020/01/hdX0nbZ3HSI.jpg
			 *
			 * The goal of this transformation is to remove domain and port from the
			 * URL so the used domain and port are the ones defined in HttpProtocol.
			 */
			transform(string => string.replaceFirst("http://(\\w+):%d".format(HttpProtocol.PORT), ""))
			saveAs("viewed_product_images")
		)
	).
	doIf("${other_products_images.exists()}"){
		/*
		 * We loop through all the images URL that we gathered above.
		 * We will use the session variable image to store the current URL.
		 */
		foreach("${other_products_images}", "image"){
			exec(
				http("Get related product image: ${image}").
				get("${image}")
			)
		}
	}.
	doIf("${viewed_product_images.exists()}"){
		exec(
			http("Get viewed image: ${viewed_product_images}").
			get("${viewed_product_images}").
			check()
		)
	}.
	doIf("${loaded_product_commons.isUndefined()}"){
		exec(
			http("Get Product style 1").
			get("/wp-content/plugins/woocommerce/assets/css/photoswipe/photoswipe.css?ver=3.8.1").
			check(regex("WooCommerce Photoswipe styles."))
		).
		exec(
			http("Get Product style 2").
			get("/wp-content/plugins/woocommerce/assets/css/photoswipe/default-skin/default-skin.css?ver=3.8.1").
			check(regex("PhotoSwipe Default UI"))
		).
		exec(
			http("Get Product script 1").
			get("/wp-content/plugins/woocommerce/assets/js/zoom/jquery.zoom.js?ver=1.7.21").
			check(regex("Zoom 1.7.21"))
		).
		exec(
			http("Get Product script 2").
			get("/wp-content/plugins/woocommerce/assets/js/flexslider/jquery.flexslider.js?ver=2.7.2").
			check(regex("jQuery FlexSlider v2.7.2"))
		).
		exec(
			http("Get Product script 3").
			get("/wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe.js?ver=4.1.1").
			check(regex("PhotoSwipe - v4.1.3"))
		).
		exec(
			http("Get Product script 4").
			get("/wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe-ui-default.js?ver=4.1.1").
			check(regex("PhotoSwipe Default UI - 4.1.3"))
		).
		exec(
			http("Get Product script 5").
			get("/wp-content/plugins/woocommerce/assets/js/frontend/single-product.js?ver=3.8.1").
			check(regex("global wc_single_product_params, PhotoSwipe, PhotoSwipeUI_Default"))
		).
		exec(
			http("Get Product script 6").
			get("/wp-includes/js/comment-reply.js?ver=5.3.2").
			check(regex("Handles the addition of the comment form."))
		).
		exec(
			http("Get Product script 7").
			get("/wp-content/themes/storefront/assets/js/sticky-add-to-cart.js?ver=2.5.3").
			check(regex("global storefront_sticky_add_to_cart_params"))
		).
		exec(_.set("loaded_product_commons", 1))
	}
}