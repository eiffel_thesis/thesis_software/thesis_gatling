/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
 * Simply do a GET request to get the account details page.
 * ALERT User needs to be connected to see this page!
 */
object SeeAccountDetails{
	final val CONNECT_PAGE_ID = 8

	// Do a simple get on account details page.
	val seeAccountDetails = exec(
		http("Account Details").
		get(s"/?page_id=$CONNECT_PAGE_ID&edit-account").
		check(css("#save-account-details-nonce", "value").saveAs("account_details_nonce")) // We save the login nonce in the session to be able to retrieve it later.
		check(regex("h1.*Account details"))
	).
	doIf("${loaded_account_details_scripts.isUndefined()}"){ // Load these scripts only if they were not loaded before.
		exec(
			http("Get Account Details script 1").
			get("/wp-includes/js/zxcvbn.min.js").
			check(regex("zxcvbn - v4.4.1"))
		).
		exec(
			http("Get Account Details script 2").
			get("/wp-includes/js/zxcvbn-async.js?ver=1.0").
			check(regex("global _zxcvbnSettings"))
		).
		exec(
			http("Get Account Details script 3").
			get("/wp-admin/js/password-strength-meter.js?ver=5.3.2").
			check(regex("global zxcvbn"))
		).
		exec(
			http("Get Account Details script 4").
			get("/wp-content/plugins/woocommerce/assets/js/frontend/password-strength-meter.js?ver=3.8.1").
			check(regex("global wp,"))
		).
		exec(_.set("loaded_account_details_scripts", 1))
	}
}