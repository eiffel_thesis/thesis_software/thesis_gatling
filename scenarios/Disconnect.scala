/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
 * Disconnect the user.
 * ALERT User needs to be already connected.
 */
object Disconnect{
	final val CONNECT_PAGE_ID = 8

	val disconnect = exec(
		http("Disconnect").
		get("/").
		// Set the query parameter to the corresponding value.
		queryParam("page_id", CONNECT_PAGE_ID).
		queryParam("customer-logout", "").
		/*
		 * Get the logout nonce from the session.
		 * It was stored in the session in the Connect scenario.
		 */
		queryParam("_wpnonce", "${logout_nonce}").
		check(regex("h1.*My account"))
	)
}