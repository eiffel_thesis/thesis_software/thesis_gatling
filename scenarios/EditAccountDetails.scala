/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.WooRandom


/**
 * Edit the account details.
 *
 * ALERT User needs to be connected to see this page, the edit_account pages
 * must have already be visited so the session contains the edit account nonce!
 *
 * @param displayName The user displayname to check that the connection
 * succeeded.
 */
class EditAccountDetails(val displayName: String){
	/**
	 * ALERT It seems that page ID are not deterministiclly set.
	 * If the creation of the site is annoyed by something (e.g. creation of user
	 * through wp-cli) thoses numbers can change...
	 */
	final val CONNECT_PAGE_ID = 8

	val newFirstName = WooRandom.randomString()

	val editAccountDetails = pause(15).exec( // Wait to let user type its modifications.
		http("Edit Account Details").
		post(s"/?page_id=$CONNECT_PAGE_ID&edit_account").
		formParamMap(Map( // Edit all the fields with random thing.
			/*
			 * Generate a random string and append it "@random.org" to form a correct
			 * mail.
			 */
			"account_email" -> WooRandom.randomMailAddress(),
			"account_first_name" -> newFirstName,
			"account_last_name" -> WooRandom.randomString(),
			"account_display_name" -> displayName,
			"action" -> "save_account_details",
			/*
			 * Get the already stored nonce to be able to edit the account details.
			 */
			"save-account-details-nonce" -> "${account_details_nonce}",
			)
		).
		/*
		 * After editing the account details user is redirected to account home so
		 * its displayName is displayed and can be used as check.
		 */
		check(regex(displayName))
	)
}