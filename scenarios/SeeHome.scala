/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.LoadCommons


/**
 * Simply do a GET request to get the home page.
 */
object SeeHome{
	/*
	 * Do a simple get on home page.
	 * Before getting the home page we load the common scripts if they were not
	 * already loaded.
	 */
	val seeHome = exec(LoadCommons.loadCommons).
	exec(http("Home").get("/").check(regex("h2.*Hello world!")))
}