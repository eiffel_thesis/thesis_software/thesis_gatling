/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
 * Simply do a GET request to get the orders page.
 * ALERT User needs to be connected to see this page!
 */
object SeeOrders{
	final val CONNECT_PAGE_ID = 8

	// This string permits us to get the orders ID.
	final val STRING_REGEX = ".*page_id=%d.*view-order=(\\d+)\">.*".format(CONNECT_PAGE_ID)

	// We transform the above String in Regex by using the .r function.
	final val REGEX = STRING_REGEX.r

	/**
	 * Check if the given string match a globally declared regular expression.
	 * @param str The string to compare with the regex. It must not contain any
	 * '\n'.
	 * @return true if the string matches the regex, false otherwise.
	 *
	 * This function will be used as condition checker for below checkIf.
	 * It takes as argument a uniline response body and compare it to a given
	 * regex.
	 * If the body matches, true is returned.
	 * Otherwise false is returned and a warning message is printed.
	 * Indeed, in our case if the user has no orders the regex will not be matched.
	 */
	def validation(str: String) : Boolean = str match {
		case REGEX(_) => true
		case _ => println(s"Body does not match '$STRING_REGEX'"); false
	}

	/**
	 * Shuffle a list.
	 * @param list The list to shuffle
	 */
	private val shuffle = (list: Seq[String]) => {
		util.Random.shuffle(list)
	}

	// Do a simple get on orders page.
	val seeOrders = exec(
		http("Orders").
		get(s"/?page_id=$CONNECT_PAGE_ID&orders").
		check(regex("h1.*Orders")).
		check(
			/*
			 * checkIf(condition)(codeIfConditionIsTrue).
			 * The condition has to be something that returns a boolean.
			 * Here we use the validation function with the response body.
			 * We replace all '\n' by simple space to be able to use regex quietly.
			 */
			checkIf((response: Response, _: Session) => validation(response.body.string.replaceAll("\n", " ")))({
				/*
				 * findAll will find all the matches that make the regex true.
				 * They will be stored as List in session variables 'orders_ids'.
				 * Before storing them we shuffle them to add some random.
				 * How to shuffle the list was taken from:
				 * https://adayinthelifeof.nl/2014/07/31/shuffling-elements-in-gatling/
				 */
				regex(STRING_REGEX).findAll.transform(shuffle).saveAs("orders_ids")
			})
		)
	)
	/*
	 * The following snippet can be used to debug session by printing its
	 * variables.
	 */
// 	.exec { session =>
// 		println("orders_ids is '" + session("orders_ids").as[String] + "'")
// 		session
// 	}
}