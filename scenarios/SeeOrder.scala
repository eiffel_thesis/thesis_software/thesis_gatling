/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
 * Simply do a GET request to one particular order.
 * ALERT User needs to be connected to see this page!
 */
class SeeOrder(val orderId: String){
	final val CONNECT_PAGE_ID = 8

	// Do a simple get on order page.
	val seeOrder = exec(
		http(s"Order ($orderId)").
		get(s"/?page_id=$CONNECT_PAGE_ID&view-order=$orderId").
		check(regex(s"h1.*Order #$orderId"))
	)
}