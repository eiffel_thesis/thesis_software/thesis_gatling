/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.WooRandom


/**
 * Add a review to a product.
 *
 * ALERT User needs to already see the product page to be able to post a review!
 *
 * @param guest Indicate either user will post as guest or as connected user.
 * ALERT In the second case, user needs to be already connected.
 */
class AddReview(val guest: Boolean){
	val comment = WooRandom.randomString()

	var postForm = Map( // Edit all the fields with random thing.
		"rating" -> WooRandom.randomRating(),
		"comment" -> comment,
		"comment_post_ID" -> "${post_id}", // Get post id in the session.
		/*
			* Hardcode 0 here because a review is not a response to an other comment.
			*/
		"comment_parent" -> 0,
	)

	/*
	 * The form fields will not contain the same information whether user is
	 * connected or will post this review as guest.
	 * We need to add some information if user is a guest.
	 */
	if(guest)
		// To concatenate two maps the operator is '++'.
		postForm = postForm ++ Map(
			"author" -> WooRandom.randomString(),
			"email" -> WooRandom.randomMailAddress(),
			"submit" -> "Submit",
		)

	val addReview = pause(15).exec( // Let the users type its review.
		http("Add Review").
		post("/wp-comments-post.php").
		formParamMap(postForm).
		check(regex(comment))
	)
}