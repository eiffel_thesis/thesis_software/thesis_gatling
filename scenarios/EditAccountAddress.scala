/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.WooRandom


/**
 * Edit the address details.
 *
 * ALERT User needs to be connected to see this page!
 *
 * @param billing Boolean used to choose between editing billing or shipping
 * address.
 */
class EditAccountAddress(val billing: Boolean){
	final val CONNECT_PAGE_ID = 8

	// Those two strings will be used to get the good editing page.
	final val BILLING = "billing"
	final val SHIPPING = "shipping"

	/*
	 * We choose between editing billing or shipping address according to billing
	 * boolean.
	 * Apparently Scala does not use the classical form of ternary operator but
	 * a more syntaxic one.
	 */
	val addressType = if(billing) BILLING else SHIPPING

	val newFirstName = WooRandom.randomString()
	val newLastName = WooRandom.randomString()

	val editAccountAddress = exec(
		http(s"Get Address Editing page ($addressType)").
		get(s"/?page_id=$CONNECT_PAGE_ID&edit-address=$addressType").
		/*
		 * We save the edit address nonce in the session to be able to retrieve it
		 * later.
		 */
		check(css("#woocommerce-edit-address-nonce", "value").saveAs("edit_address_nonce"))
		check(regex(s"h3.*${addressType.capitalize}"))
	).
	pause(15).
	exec( // Wait to let user type its modifications.
		http(s"Edit Account Address ($addressType)").
		post(s"/?page_id=$CONNECT_PAGE_ID&edit-address=$addressType").
		formParamMap(Map( // Edit all the fields with random thing.
			s"${addressType}_first_name" -> newFirstName,
			s"${addressType}_last_name" -> newLastName,
			/*
			 * Harcode France here because postcode must be in accordance with
			 * country.
			 */
			s"${addressType}_country" -> "FR",
			s"${addressType}_address_1" -> WooRandom.randomString(),
			s"${addressType}_city" -> WooRandom.randomString(),
			/*
			 * French ZIP code have 5 numbers.
			 * It is not possible to have postcode inferior to 01000 but WooCommerce
			 * seems to accept the ZIP code 00000 so the following snippet is OK.
			 * The snippet will generate a random number in [0, 99999] and add 0 so
			 * the string length is 5 (e.g., 5 will be expand to 00005).
			 */
			s"${addressType}_postcode" -> WooRandom.randomFrenchPostCode(),
			// Do the same for the phone.
			s"${addressType}_phone" -> WooRandom.randomPhoneNumber(),
			s"${addressType}_email" -> WooRandom.randomMailAddress(),
			"save_address" -> "Save address",
			"woocommerce-edit-address-nonce" -> "${edit_address_nonce}",
			"action" -> "edit_address",
			)
		).
		/*
		 * After editing the address user is redirected to address page so its
		 * newFirstName is displayed and can be used as check.
		 */
		check(regex(s"$newFirstName $newLastName"))
	)
}