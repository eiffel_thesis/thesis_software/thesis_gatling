/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.WooRandom
import woocommerce.common.LoadCommons


/**
 * Buy the articles present in the cart.
 *
 * ALERT The cart must not be empty.
 *
 * @param guest true if the client is a guest, false otherwise.
 */
class Checkout(guest: Boolean){
	final val CHECKOUT_PAGE_ID = 7

	final val GUEST_DURATION = 20
	final val USER_DURATION = 5

	// Set the form fields with random generated values.
	val guestForm = Map(
		"billing_first_name" -> WooRandom.randomString(),
		"billing_last_name" -> WooRandom.randomString(),
		"billing_address_1" -> WooRandom.randomString(),
		/*
		 * French ZIP code have 5 numbers.
		 * It is not possible to have postcode inferior to 01000 but WooCommerce
		 * seems to accept the ZIP code 00000 so the following snippet is OK.
		 * The snippet will generate a random number in [0, 99999] and add 0 so
		 * the string length is 5 (e.g., 5 will be expand to 00005).
		 */
		"billing_postcode" -> WooRandom.randomFrenchPostCode(),
		"billing_city" -> WooRandom.randomString(),
		/*
		 * Generate a random phone number with 10 numbers.
		 * If, for example, the Random number is 15 it will be extended to
		 * 0000000015.
		 */
		"billing_phone" -> WooRandom.randomPhoneNumber(),
		"billing_email" -> WooRandom.randomMailAddress()
	)

	// Set the form fields with session variables.
	val userForm = Map(
		"billing_first_name" -> "${checkout_firstname}",
		"billing_last_name" -> "${checkout_lastname}",
		"billing_address_1" -> "${checkout_address}",
		"billing_postcode" -> "${checkout_postcode}",
		"billing_city" -> "${checkout_city}",
		"billing_phone" -> "${checkout_phone}",
		"billing_email" -> "${checkout_email}"
	)

	// Use the good form based on guest.
	var form = if(guest) guestForm else userForm

	form += (
	// Add the checkout nonce to the form.
	"woocommerce-process-checkout-nonce" -> "${checkout_nonce}",
	/*
	 * Harcode France here because either user is a guest and its postcode will be
	 * randomly generated so it must be in accordance with country.
	 * Or the user is connected but it is hard to get its country so to simplify
	 * I put FR.
	 */
	"billing_country" -> "FR",
	)

	/*
	 * If user is a guest it will take some time to type its information.
	 * Otherwise fields are already filled so pause will be shorter.
	 */
	val pauseDuration = if(guest) GUEST_DURATION else USER_DURATION

	/*
	 * Do a simple get on checkout page.
	 * We first load the common script and stylecheet files if they were not
	 * already loaded.
	 */
	val checkout = exec(LoadCommons.loadCommons).
	exec(http("See Checkout").
		get(s"/?page_id=$CHECKOUT_PAGE_ID").
		check(regex("h1.*Checkout")).
		// Save the nonce to be able to post the form.
		check(css("#woocommerce-process-checkout-nonce", "value").saveAs("checkout_nonce")).
		/*
		 * Then we save all the important fields of the form in session variables.
		 * Indeed, if the user is already connected those fields are filled with
		 * his/her information.
		 * If the user is a guest those fields will be empty and the session
		 * variable set to empty thing but this is not a big problem since we will
		 * use random data form guest.
		 */
		check(css("#billing_first_name", "value").saveAs("checkout_firstname")).
		check(css("#billing_last_name", "value").saveAs("checkout_lastname")).
		check(css("#billing_address_1", "value").saveAs("checkout_address")).
		check(css("#billing_postcode", "value").saveAs("checkout_postcode")).
		check(css("#billing_city", "value").saveAs("checkout_city")).
		check(css("#billing_phone", "value").saveAs("checkout_phone")).
		check(css("#billing_email", "value").saveAs("checkout_email"))
	).
	doIf("${loaded_checkout_script.isUndefined()}"){
		exec(
			http("Get Checkout script").
			get("/wp-content/plugins/woocommerce/assets/js/frontend/checkout.js?ver=3.8.1").
			check(regex("global wc_checkout_params"))
		).
		exec(_.set("loaded_checkout_script", 1))
	}.
	pause(pauseDuration).
	exec(http("Do Checkout").
		post("?wc-ajax=checkout").
		formParamMap(form).
		/*
		 * I did not activate payement method in WooCommerce so when user clicked on
		 * "Place order" it prints an "Invalid payement method" which is given
		 * through JSON.
		 * So we just check the JSON which was responded.
		 */
		check(jsonPath("$..result").is("failure"))
	)
}