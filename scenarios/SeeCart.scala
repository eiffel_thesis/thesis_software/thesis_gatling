/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.scenarios
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.LoadCommons
import woocommerce.common.HttpProtocol


/**
 * Simply do a GET request to get the cart page.
 */
object SeeCart{
	final val CART_PAGE_ID = 6

	// Do a simple get on cart page.
	val seeCart = exec(LoadCommons.loadCommons).
	exec(
		http("Cart").
		get(s"/?page_id=$CART_PAGE_ID").
		check(regex("h1.*Cart")).
		check(
			/*
			 * Gatling css selector is based on:
			 * https://jodd.org/csselly/
			 * We want to get the image markup (<img>) whom class attribute contains
			 * "attachment-woocommerce_thumbnail".
			 * So, we do this by using class=~attachment-woocommerce_thumbnail.
			 */
			css("""img[class~="attachment-woocommerce_thumbnail"]""", "src").
			findAll.
			transform(_.map(_.replaceFirst("http://(\\w+):%d".format(HttpProtocol.PORT), ""))).
			// The cart can be empty. So we mark this check as optional.
			optional.
			// We save all the URL into session variable if cart is not empty.
			saveAs("cart_images")
		)
	).
	doIf("${cart_images.exists()}"){
		/*
		 * We loop through all the images URL that we gathered above.
		 * We will use the session variable image to store the current URL.
		 */
		foreach("${cart_images}", "image"){
			exec(
				http("Get cart image: ${image}").
				get("${image}")
			)
		}
	}.
	doIf("${loaded_cart_script.isUndefined()}"){
		exec(
			http("Get Cart script").
			get("/wp-content/plugins/woocommerce/assets/js/frontend/cart.js?ver=3.8.1")
			.check(regex("global wc_cart_params"))
		).
		exec(_.set("loaded_cart_script", 1))
	}
}