/*
 * Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
 * SPDX-License-Identifier: Apache-2.0
 */
package woocommerce.exps
import scala.io.Source
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import woocommerce.common.ScenarioJSONParser


/**
 * Do the experiment.
 */
class Experiment extends Simulation{
	/**
	 * This CSV file contains products names.
	 *
	 * It was obtained by first running those commands to add products:
	 * wp cyclone seed --allow-root
	 * wp cyclone products 30000 --allow-root
	 *
	 * The product names are obtained with the following command and stored into
	 * CSV file:
	 * wp db query "select post_name from wp_posts where post_type = 'product';" --allow-root
	 *
	 * CSV file looks like:
	 * product_name
	 * in-accusamus
	 * incidunt-qui
	 * tenetur-rerum
	 */
	final val FACTORY_PRODUCTS_FILE = "user-files/resources/ExperimentProducts.csv"

	/**
	 * This CSV file contains usernames and their displaynames.
	 *
	 * We first add users by adding orders:
	 * wp cyclone orders 5000 --allow-root
	 *
	 * We get the user names and the associated display name by using this
	 * command:
	 * wp db query "select user_login, display_name from wp_users;" --allow-root
	 *
	 * Those data are stored into CSV file which looks like:
	 * username,displayname
	 * chet.cruickshank,Margie Schumm
	 * kip.klocko,Xzavier Haag
	 * laurie.kuvalis,Frances Kling
	 */
	final val FACTORY_USERS_FILE = "user-files/resources/ExperimentUsers.csv"

	/**
	 * This file contains our scenario and will be parsed.
	 */
	final val JSON_FILE = "user-files/resources/Experiment.json"

	/**
	 * Parse the FACTORY_PRODUCTS_FILE and store all its items into an array
	 * Heavily inspired from:
	 * https://alvinalexander.com/scala/csv-file-how-to-process-open-read-parse-in-scala
	 * @return An array of product names.
	 */
	private def parseProductsCSV(): Array[String] = {
		var ret = new Array[String](0)

		// Open the file.
		val bufferedSource = Source.fromFile(FACTORY_PRODUCTS_FILE)

		/*
		 * Loop through each line of the file.
		 * drop(1) permits to drop the header line.
		 */
		for (line <- bufferedSource.getLines.drop(1)) {
				/*
				 * Since there is only one column in our CSV file we can add the whole
				 * line.
				 */
				ret = Array.concat(ret, Array(line))
		}

		bufferedSource.close()

		return ret
	}

	/**
	 * Parse the FACTORY_USERS_FILE and store the usernames and display names into
	 * arrays.
	 * @return A tuple of two arrays whom first contains all usernames and second
	 * all displaynames.
	 */
	private def parseUsersCSV(): (Array[String], Array[String]) = {
		var users = new Array[String](0)
		var displays = new Array[String](0)

		val bufferedSource = Source.fromFile(FACTORY_USERS_FILE)

		for (line <- bufferedSource.getLines.drop(1)) {
				// Split the string by ','
				val cols = line.split(",").map(_.trim)

				// Add the first field to usernames array and second to displaynames.
				users = Array.concat(users, Array(cols(0)))
				displays = Array.concat(displays, Array(cols(1)))
		}

		bufferedSource.close()

		return (users, displays)
	}

	final val FACTORY_PRODUCT_NAMES = parseProductsCSV()

	val ret = parseUsersCSV()

	// Access the returned tuple
	final val FACTORY_USER_NAMES = ret._1
	final val FACTORY_DISPLAYED_NAMES = ret._2

	/**
	 * The equation to set FACTORY_LAST_PAGE_NUMBER is:
	 * FACTORY_LAST_PAGE_NUMBER = number_of_products / 12 + number_of_products % 12 ? 1 : 0
	 * Because there are 12 products per page and always one page even if there
	 * are less than 12 products.
	 */
	final val FACTORY_LAST_PAGE_NUMBER = 2500

	/**
	 * FACTORY_LAST_PRODUCT_ID was set by using:
	 * wp db query "select id from wp_posts where post_type = 'product';" --allow-root
	 * It seems that ID are always even and begins from 9.
	 * The last one was captured with the SQL above.
	 */
	final val FACTORY_LAST_PRODUCT_ID = 60007

	// Read the whole file at once.
	final val JSON = Source.fromFile(JSON_FILE).mkString

	// We instantiate a JSON Parser for those data.
	val scenarioJSONParser = new ScenarioJSONParser(FACTORY_USER_NAMES, FACTORY_DISPLAYED_NAMES, FACTORY_PRODUCT_NAMES, FACTORY_LAST_PAGE_NUMBER, FACTORY_LAST_PRODUCT_ID)

	setUp(scenarioJSONParser.scenarioJSONParser(JSON))
}